
const events = require('./data/events.json')

module.exports = [
  {
    url: '/demo/data/events.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  events
      }
    }
  }
]
