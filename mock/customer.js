
const customers_small = require('./data/customers-small.json')
const customers_medium = require('./data/customers-medium.json')
const customers_large = require('./data/customers-large.json')
const customers_xlarge = require('./data/customers-xlarge.json')


module.exports = [
  {
    url: '/demo/data/customers-small.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  customers_small
      }
    }
  },
  {
    url: '/demo/data/customers-medium.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  customers_medium
      }
    }
  },
  {
    url: '/demo/data/customers-large.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  customers_large
      }
    }
  },
  {
    url: '/demo/data/customers-xlarge.json',
    type: 'get',
    response: _ => {
      return {
        code: 20000,
        data:  customers_xlarge
      }
    }
  }
]
